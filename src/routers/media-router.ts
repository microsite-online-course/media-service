import BaseRouter from '@/routers/base-router';
import { GetMediaController, StoreMediaController, DeleteMediaController } from '@/controllers/media';

class MediaRoutes extends BaseRouter {
  public routes(): void {
    this.router.get('/', GetMediaController);
    this.router.post('/', StoreMediaController);
    this.router.delete('/:id', DeleteMediaController);
  }
}

export default new MediaRoutes().router;
