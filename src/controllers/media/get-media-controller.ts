import { Request, Response } from 'express';
import Media from '@/db/models/media';

interface QueryParams {
  page: number;
}

type SqlOptions = {
  attributes: string[];
  offset?: number;
  limit?: number;
};

const GetMediaController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page } = req.query as unknown as QueryParams;

    const pageSize = 25;
    const offset = ((page || 1) - 1) * pageSize;
    const count = await Media.count();

    const sqlOptions: SqlOptions = {
      attributes: ['id', 'image'],
    };

    sqlOptions.offset = offset;
    sqlOptions.limit = pageSize;

    const media = await Media.findAll(sqlOptions);

    const mappedMedia = media.map((row: { id: string; image: string }) => {
      row.image = `${req.get('host')}/${row.image}`;
      return row;
    });

    return res.json({
      status: 'success',
      metadata: {
        page: page as number,
        per_page: pageSize,
        total_page: Math.ceil(count / pageSize),
      },
      data: mappedMedia,
    });
  } catch (error) {
    return res.status(500).json({
      status: 'error',
      message: error,
    });
  }
};

export default GetMediaController;
