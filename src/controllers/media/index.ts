import GetMediaController from '@/controllers/media/get-media-controller';
import StoreMediaController from '@/controllers/media/store-media-controller';
import DeleteMediaController from '@/controllers/media/delete-media-controller';

export { GetMediaController, StoreMediaController, DeleteMediaController };
