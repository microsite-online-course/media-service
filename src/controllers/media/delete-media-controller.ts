import { Request, Response } from 'express';
import Media from '@/db/models/media';
const fs = require('fs');

const DeleteMediaController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const id = req.params.id;
    const media = await Media.findByPk(id);

    if (!media) {
      return res.status(400).json({
        status: 'error',
        message: 'media not found',
      });
    }

    fs.unlink(`./public/${media.image}`, async (err: any) => {
      if (err) {
        return res.status(400).json({
          status: 'error',
          message: err.message,
        });
      }

      await media.destroy();
    });

    return res.json({
      status: 'success',
      message: 'delete media successfully',
    });
  } catch (error) {
    return res.status(500).json({
      status: 'error',
      message: error,
    });
  }
};

export default DeleteMediaController;
