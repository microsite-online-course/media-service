import { Request, Response } from 'express';
import Media from '@/db/models/media';
import isBase64 from 'is-base64';
import { converBase64ToImage } from 'convert-base64-to-image';

const StoreMediaController = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { image } = req.body;
    const imageName = `${Date.now()}.png`;
    const pathToSaveImage = `./public/images/${imageName}`;

    if (!isBase64(image, { mimeRequired: true })) {
      return res.status(400).json({
        status: 'error',
        message: 'invalid base 64',
      });
    }

    converBase64ToImage(image, pathToSaveImage);

    const media = await Media.create({
      image: `images/${imageName}`,
    });

    return res.json({
      status: 'success',
      data: {
        id: media.id,
        image: `${req.get('host')}/images/${imageName}`,
      },
    });
  } catch (error) {
    return res.status(500).json({
      status: 'error',
      message: error,
    });
  }
};

export default StoreMediaController;
