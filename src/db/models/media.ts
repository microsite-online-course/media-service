import { DataTypes, Model, Optional } from 'sequelize';
import dbConnect from '@/config/db-connect';

interface MediaAttributes {
  id?: string;
  image?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface MediaInput extends Optional<MediaAttributes, 'id'> {}

export interface MediaOutput extends Required<MediaAttributes> {}

class Media extends Model<MediaAttributes, MediaInput> implements MediaAttributes {
  public id!: string;
  public image!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Media.init(
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    image: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  },
  { sequelize: dbConnect, freezeTableName: true, underscored: true, timestamps: true, tableName: 'media' },
);

export default Media;
